function ShowBelize() {
    document.getElementById("belize").style.display = "block";
    document.getElementById("costarica").style.display = "none";
    document.getElementById("elsalvador").style.display = "none";
    document.getElementById("guatemala").style.display = "none";
    document.getElementById("honduras").style.display = "none";
    document.getElementById("nicaragua").style.display = "none";
    document.getElementById("panama").style.display = "none";
}
function ShowCostaRica() {
    document.getElementById("belize").style.display = "none";
    document.getElementById("costarica").style.display = "block";
    document.getElementById("elsalvador").style.display = "none";
    document.getElementById("guatemala").style.display = "none";
    document.getElementById("honduras").style.display = "none";
    document.getElementById("nicaragua").style.display = "none";
    document.getElementById("panama").style.display = "none";
}
function ShowSalvador() {
    document.getElementById("belize").style.display = "none";
    document.getElementById("costarica").style.display = "none";
    document.getElementById("elsalvador").style.display = "flex";
    document.getElementById("guatemala").style.display = "none";
    document.getElementById("honduras").style.display = "none";
    document.getElementById("nicaragua").style.display = "none";
    document.getElementById("panama").style.display = "none";
}
function ShowGuatemala() {
    document.getElementById("belize").style.display = "none";
    document.getElementById("costarica").style.display = "none";
    document.getElementById("elsalvador").style.display = "none";
    document.getElementById("guatemala").style.display = "flex";
    document.getElementById("honduras").style.display = "none";
    document.getElementById("nicaragua").style.display = "none";
    document.getElementById("panama").style.display = "none";
}
function ShowHonduras() {
    document.getElementById("belize").style.display = "none";
    document.getElementById("costarica").style.display = "none";
    document.getElementById("elsalvador").style.display = "none";
    document.getElementById("guatemala").style.display = "none";
    document.getElementById("honduras").style.display = "flex";
    document.getElementById("nicaragua").style.display = "none";
    document.getElementById("panama").style.display = "none";
}
function ShowNicaragua() {
    document.getElementById("belize").style.display = "none";
    document.getElementById("costarica").style.display = "none";
    document.getElementById("elsalvador").style.display = "none";
    document.getElementById("guatemala").style.display = "none";
    document.getElementById("honduras").style.display = "none";
    document.getElementById("nicaragua").style.display = "flex";
    document.getElementById("panama").style.display = "none";
}
function ShowPanama() {
    document.getElementById("belize").style.display = "none";
    document.getElementById("costarica").style.display = "none";
    document.getElementById("elsalvador").style.display = "none";
    document.getElementById("guatemala").style.display = "none";
    document.getElementById("honduras").style.display = "none";
    document.getElementById("nicaragua").style.display = "none";
    document.getElementById("panama").style.display = "flex";
}